;+
; NAME:
;   estimate_pxf_kin_err.pro
;
; PURPOSE:
;   Quick estimate of statistical errors of kinematic parameters of galaxies
;   obtained using pixel space fitting of their absorption line spectra with
;   a template spectrum, e.g. using PPxF by M.Cappellari. It takes a
;   template spectrum, an estimated signal-to-noise ratio (in every pixel
;   or a median value for the whole spectrum), and expected velocity
;   dispersion and computes the uncertainties assuming no template mismatch.
;
; INPUTS:
;   wl         - wavelength scale of a template spectrum, 1D array (Npix)
;   template   - template spectrum convolved with the LSF, 1D array (Npix)
;   sigma      - velocity dispersion estimate, scalar value (km/s)
;   snr        - signal-to-noise ratio, either scalar or 1D array (Npix)
;
; OPTIONAL INPUTS:
;   /direct    - a keyword to force pixel-space rather than Fourier-space 
;                convolution
; OUTPUTS:
;   result     - a two-element array with estimated uncertainties of radial
;                velocity and velocity dispersion
;
; COMMENTS:
;   1. "template" IS NOT an observed spectrum of a galaxy but rather its 
;   model with the velocity dispersion of 0 but convolved to match a spectral
;   resolution of a galaxy spectrum (i.e. a template spectrum as it would be
;   passed to PPxF or similar routines).
;
;   2. "snr" vector can be computed from observed data as flux/flux_err. In
;   this case the multiplicative continuum from PPxF or NBursts does not 
;   affect the calculation.
;
; EXAMPLE:
;   TBD
;
; REVISION HISTORY:
;   2019-Dec-08 - First public IDL version by Igor Chilingarian and 
;                 first public Python version by Kirill Grishin
;----------------------------------------------------------------------

function estimate_pxf_kin_err,wl,template,sigma,snr,direct=direct
    n_wl=n_elements(wl)
    velScale=((wl[n_wl/2+1]-wl[n_wl/2])/wl[n_wl/2]*299792.478d)
    sigma_pix=sigma/velScale
    template_norm=template
    if(n_elements(template) ne n_wl) then $
        message,'template spectrum has a wrong size'
    if(n_elements(snr) ne 1 and n_elements(snr) ne n_wl) then $
        message,'SNR vector has a wrong size'
    errtmp=median(template)/snr

    if(sigma_pix lt 0.8d and keyword_set(direct)) then message,/inf,'Undersampled kernel (sigma ='+string(sigma_pix,format='(f5.2)')+'pix), use Fourier space convolution'
    if(keyword_set(direct)) then begin
        n2_krnl=ceil((sigma_pix*8)>3)
        krnl=psf_gaussian(ndim=1,npix=2*n2_krnl+1,st_dev=sigma_pix,/norm,/double)
        krnl1=krnl*((dindgen(2*n2_krnl+1)-n2_krnl)/sigma_pix)
        krnl2=krnl*((dindgen(2*n2_krnl+1)-n2_krnl)^2/sigma_pix^2)

        vel_err_pix=sigma_pix/$
            sqrt(total((convol(template_norm,krnl1,/edge_trunc))^2/errtmp^2))
        sigma_err_pix=sigma_pix/$
            sqrt(total((convol(template_norm,krnl,/edge_trunc)-$
                        convol(template_norm,krnl2,/edge_trunc))^2/errtmp^2))
    endif else begin ;; fourier space convolution
        n_tf=2l^(ceil(alog(n_wl)/alog(2)))
        if(n_tf gt n_wl) then begin ;; expanding the template to 2^n pix
            template_2=dblarr(n_tf) 
            template_2[0:n_wl-1]=template_norm
            template_2[n_wl:*]=template_norm[n_wl-1]+$
                dindgen(n_tf-n_wl)*(template_norm[0]-template_norm[n_wl-1])/(n_tf-n_wl)
        endif else template_2=template_norm
        template_2_f=fft(template_2,-1)

        w_f=2d*!dpi*(dindgen(n_tf)-n_tf/2+1)/n_tf
        g_f=exp(-0.5d*sigma_pix^2*w_f^2)
        krnl_f =dcomplex(shift(g_f,n_tf/2-1),dblarr(n_tf))
        krnl1_f=dcomplex(dblarr(n_tf),shift(sigma_pix*w_f*g_f,n_tf/2l -1l))
        krnl2_f=dcomplex(shift(g_f*(1d - w_f^2*sigma_pix^2),n_tf/2l -1l),dblarr(n_tf))

        vel_err_pix=sigma_pix/sqrt(total((real_part(fft(template_2_f*krnl1_f,1)))[0:n_wl-1]^2/errtmp^2))
        sigma_err_pix=sigma_pix/sqrt(total((real_part(fft(template_2_f*(krnl_f-krnl2_f),1)))[0:n_wl-1]^2/errtmp^2))
    endelse

    return, [vel_err_pix,sigma_err_pix]*velScale
end
