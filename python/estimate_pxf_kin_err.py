import numpy as np
import warnings

def psf_gaussian(npix,st_dev):
    nn = (npix - 1)/2
    x = np.linspace(-nn, nn, 2*nn+1)
    yy = x/st_dev
    kern = 1.0 / np.sqrt(2*np.pi) / st_dev * np.exp(-0.5*yy**2)
    kern = kern / np.sum(kern)
    return kern

def estimate_pxf_kin_err(wl,template,sigma,snr,direct=True):
    """
    NAME:
    estimate_pxf_kin_err

    PURPOSE:
    Quick estimate of statistical errors of kinematic parameters of galaxies
    obtained using pixel space fitting of their absorption line spectra with
    a template spectrum, e.g. using PPxF by M.Cappellari. It takes a
    template spectrum, an estimated signal-to-noise ratio (in every pixel
    or a median value for the whole spectrum), and expected velocity
    dispersion and computes the uncertainties assuming no template mismatch.

    INPUTS:
    wl         - wavelength scale of a template spectrum, 1D array (Npix)
    template   - template spectrum convolved with the LSF, 1D array (Npix)
    sigma      - velocity dispersion estimate, scalar value (km/s)
    snr        - signal-to-noise ratio, either scalar or 1D array (Npix)

    OPTIONAL INPUTS:
    direct    - a keyword to force pixel-space rather than Fourier-space 
                    convolution
    OUTPUTS:
    result     - a two-element tuple with estimated uncertainties of radial
                    velocity and velocity dispersion

    COMMENTS:
    1. "template" IS NOT an observed spectrum of a galaxy but rather its 
    model with the velocity dispersion of 0 but convolved to match a spectal
    resolution of a galaxy spectrum (i.e. a template spectrum as it would be
    passed to PPxF or similar routines).

    2. "snr" vector can be computed from observed data as flux/flux_err. In
    this case the multiplicative continuum from PPxF or NBursts does not 
    affect the calculation.

    EXAMPLE:

    REVISION HISTORY:
    2019-Dec-08 - First public IDL version by Igor Chilingarian and 
                    first public Python version by Kirill Grishin

    """
    n_wl = len(wl)
    velScale = ((wl[int(n_wl/2)+1]-wl[int(n_wl/2)])/wl[int(n_wl/2)]*299792.478)
    sigma_pix = sigma/velScale
    template_norm = template
    if len(template) != len(wl):
        raise Exception('Template spectrum has a wrong size')
    try:
        errtmp = np.median(template)/snr
        err_pxl = template/snr
    except ValueError:
        raise Exception('Error: SNR vector has a wrong size')
    if((sigma_pix < 0.8) & direct): 
        warnings.warn('Undersampled kernel (sigma =%5.2f pix), use Fourier space convolution' % sigma_pix)
    if direct:
        n2_krnl = np.ceil(sigma_pix*8)
        n2_krnl = 3 if n2_krnl < 3 else n2_krnl
        krnl = psf_gaussian(npix=2*n2_krnl+1,st_dev=sigma_pix)
        krnl1 = krnl*((np.arange(2*n2_krnl+1)-n2_krnl)/sigma_pix)
        krnl2 = krnl*((np.arange(2*n2_krnl+1)-n2_krnl)**2/sigma_pix**2)
        vel_err_pix = sigma_pix/np.sqrt(np.sum((np.convolve(template_norm, krnl1, mode='valid'))**2/errtmp**2))
        sigma_err_pix = sigma_pix/np.sqrt(np.sum((np.convolve(template_norm, krnl, mode='valid')-
                        np.convolve(template_norm, krnl2, mode='valid'))**2)/errtmp**2)
    else:
        n_tf = 2**(np.ceil(np.log(n_wl)/np.log(2.0)))
        n_tf = int(n_tf)
        if (n_tf > n_wl):
            template_2 = np.zeros(n_tf, dtype=np.float) 
            template_2[0:n_wl] = template_norm
            template_2[n_wl:] = template_norm[n_wl-1] + np.arange(n_tf-n_wl)*(template_norm[0] - template_norm[n_wl-1])/(n_tf-n_wl)
        else:
            template_2 = template_norm
        template_2_f = np.fft.ifft(template_2)
        w_f = 2.0 * np.pi * (np.arange(n_tf)-int(n_tf/2) + 1.0)/n_tf
        g_f = np.exp(-0.5*sigma_pix**2*w_f**2)
        krnl_f = np.roll(g_f,int(n_tf/2)-1) + 1j * np.zeros(n_tf)
        krnl1_f = np.zeros(n_tf) + np.roll(sigma_pix*w_f*g_f,int(n_tf/2)-1) * 1.0j
        krnl2_f = np.roll(g_f*(1.0 - w_f**2*sigma_pix**2),int(n_tf/2)-1) + 1.0j * np.zeros(n_tf)
        vel_err_pix = sigma_pix/np.sqrt(np.sum((np.real(np.fft.fft(template_2_f*krnl1_f)))[0:n_wl]**2/errtmp**2))
        sigma_err_pix = sigma_pix/np.sqrt(np.sum((np.real(np.fft.fft(template_2_f*(krnl_f-krnl2_f))))[0:n_wl]**2/errtmp**2))
    return vel_err_pix*velScale, sigma_err_pix*velScale
